/*
 * ble.c
 *
 *  Created on: 25 feb. 2019
 *      Author: Antoni
 */

#include "ble.h"
#include "ctes.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
//#include "esp_system.h"
#include "esp_log.h"
#include "freertos/event_groups.h"
#include "esp_system.h"

#include "string.h"
#include <stdio.h>
#include "esp_event_loop.h"
#include "uart_ble.h"
#include "main.h"

uint8_t BLE_STR_BLE_2_AV_BOOT[3] = {0x01, 0x21, 0x02};
uint8_t BLE_STR_BLE_CONNECTED[3] = {0x01, 0x22, 0x02};
uint8_t BLE_STR_BLE_DISCONNECTED[3] ={0x01, 0x23, 0x02};
uint32_t BleStMachine = 0, BleProgFlags = 0, BleProgParserPtr = 0, BleProgCurrentParam, BleProgConnected;
uint8_t BleProgParser[BLE_DATA_LONG];

extern xQueueHandle UartIoTfromAvisadorQueue;


/* ******************************************************************************************** */
/**
 * @brief Parseo del dato recibido por la uart que comunica con el stm32
 * @param
 * @retval
 */
void ble_con_parser (uint8_t dato)
{
	switch (dato)
	{
		case BLE_COM_FRAME_INI:
		{
			BleProgParserPtr = 0;
			BleProgParser[0] = '\0'; BleProgFlags = 0;
			SET_FLAG (BleProgFlags, BLE_PROG_FLG_INI);
		}
		break;
		case BLE_COM_FRAME_FIN:
		{
			if (TST_FLAG (BleProgFlags, BLE_PROG_FLG_INI))
			{
				if (BleProgCurrentParam == BLE_CMD_AVIS_BOOT)
					SET_FLAG (BleProgFlags, BLE_PROG_FLG_AVIS_BOOT);
			}
		}
		break;
		default:
		{
			if (!BleProgParserPtr && TST_FLAG (BleProgFlags, BLE_PROG_FLG_INI))
			{
				BleProgParserPtr++;
				if (dato == BLE_CMD_AVIS_BOOT)
					BleProgCurrentParam = dato;
			}
			else if (BleProgParserPtr && TST_FLAG (BleProgFlags, BLE_PROG_FLG_INI))
			{
				BleProgParser[BleProgParserPtr++ - 1] = dato;
			}
			break;
		}
	}
}
/* ******************************************************************************************** */
/**
 * @brief
 * @param
 * @retval
 */
void ble_gat_connected(uint32_t st)
{
	BleProgConnected = st;
}
/* ******************************************************************************************** */
/**
 * @brief Tarea que gestiona el estado del ble
 * @param
 * @retval
 */
void ble_state_machine_task()
{
	static const char *TX_TASK_TAG = "ble_state_machine_task";
	uint8_t car;
	int ptr = 0;
	uint8_t* data = (uint8_t*) malloc(BLE_DATA_LONG + 1);
	while (1)
	{
		uint32_t tam = uxQueueMessagesWaiting(UartIoTfromAvisadorQueue);
		if (tam)
		{
			ptr = 0;
			while (xQueueReceive (UartIoTfromAvisadorQueue, &car, 1) != errQUEUE_EMPTY)
			{
				if (BleStMachine == BLE_ST_BLE_CONNECTED)
					data[ptr++] = car;
				else
					ble_con_parser(car);
			}
			if (BleStMachine == BLE_ST_BLE_CONNECTED)
			{
				if (ptr)
				{
					esp_log_buffer_hex(TX_TASK_TAG, data, tam);
					ble_indicate(data, tam);
				}
			}
		}
		switch (BleStMachine)
		{
		case BLE_ST_BLE_BOOT:
		{
			//En cuanto recibimos el comando desde el stm32 0x20 le enviamos el comando 0x21, significa que el esp32 ha arrancado correctamente
			if (TST_FLAG (BleProgFlags, BLE_PROG_FLG_AVIS_BOOT))
			{
				flash_custom_data_set(BleProgParser);
				uart_iot_send_str_l (BLE_STR_BLE_2_AV_BOOT, 3);
				CLR_FLAG (BleProgFlags, BLE_PROG_FLG_AVIS_BOOT);
				BleStMachine = BLE_ST_BLE_AVIS_UP;
			}
		}
		break;
		case BLE_ST_BLE_AVIS_UP:
		{
			if (TST_FLAG (BleProgFlags, BLE_PROG_FLG_AVIS_BOOT))
			{
				flash_custom_data_set(BleProgParser);
				uart_iot_send_str_l (BLE_STR_BLE_2_AV_BOOT, 3);
				CLR_FLAG (BleProgFlags, BLE_PROG_FLG_AVIS_BOOT);
				BleStMachine = BLE_ST_BLE_AVIS_UP;
			}
			if (BleProgConnected)
			{
				uart_iot_send_str_l (BLE_STR_BLE_CONNECTED, 3);
				BleStMachine = BLE_ST_BLE_CONNECTED;
			}
		}
		break;
		case BLE_ST_BLE_CONNECTED:
		{
			if (!BleProgConnected)
			{
				uart_iot_send_str_l (BLE_STR_BLE_DISCONNECTED, 3);
				BleStMachine = BLE_ST_BLE_DISCONNECTED;
			}
		}
		break;
		case BLE_ST_BLE_DISCONNECTED:
		{
			BleStMachine = BLE_ST_BLE_AVIS_UP;
		}
		break;

		}
		vTaskDelay(10 / portTICK_PERIOD_MS);
	}
}

