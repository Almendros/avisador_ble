/*
 * ctes.h
 *
 *  Created on: 28 nov. 2018
 *      Author: Antoni
 */

#ifndef MAIN_CTES_H_
#define MAIN_CTES_H_

#define SET_FLAG(ADDRESS,MASK) ((ADDRESS) |= (MASK))
#define CLR_FLAG(ADDRESS,MASK) ((ADDRESS) &= ~(MASK))
#define TGL_FLAG(ADDRESS,MASK) ((ADDRESS) ^= MASK)
#define TST_FLAG(ADDRESS,MASK) ((ADDRESS) & MASK)
#define TST_ALL_FLAGS(ADDRESS,MASK) ((TST_FLAG(ADDRESS,MASK) == MASK) ? 1 : 0)


enum
{
	FALSE = 0,
	TRUE = 1,
	TRUE_ELSE = 2,
	MAYBE = 3
};



#endif /* MAIN_CTES_H_ */
