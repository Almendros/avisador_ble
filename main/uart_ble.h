/*
 * uart_iot.h
 *
 *  Created on: 21 ene. 2019
 *      Author: Antoni
 */

#ifndef MAIN_UART_BLE_H_
#define MAIN_UART_BLE_H_


#include "ctes.h"
#include "esp_system.h"
#include "driver/uart.h"
#include "soc/uart_struct.h"
#include "string.h"
#include <stdio.h>
#include "freertos/event_groups.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include <driver/gpio.h>
#include "esp_log.h"


//GLOBALES GATT
#define GATTS_TAG "AVISADOR BLE"
#define GATTS_SERVICE_UUID_TEST_A   0x00FF
#define GATTS_CHAR_UUID_TEST_A      0xFF01
#define GATTS_DESCR_UUID_TEST_A     0x3333
#define GATTS_NUM_HANDLE_TEST_A     4

#define GATTS_SERVICE_UUID_TEST_B   0x00EE
#define GATTS_CHAR_UUID_TEST_B      0xEE01
#define GATTS_DESCR_UUID_TEST_B     0x2222
#define GATTS_NUM_HANDLE_TEST_B     4

#define TEST_DEVICE_NAME            "SOLINGEL TECH"
#define TEST_MANUFACTURER_DATA_LEN  17

#define GATTS_DEMO_CHAR_VAL_LEN_MAX 0x40

#define PREPARE_BUF_MAX_SIZE 1024
//////////////////////////////////////////////////////


#define RX_BUF_SIZE  		512
#define IOT_DATA_LONG  		512



#define TXD_PIN (GPIO_NUM_2)
#define RXD_PIN (GPIO_NUM_27)

#define GPIO_BG96_PWRKEY    13
#define GPIO_BG96_RESET     15
#define GPIO_BG96_PIN_SEL  ((1ULL<<GPIO_BG96_PWRKEY) |  (1ULL<<GPIO_BG96_RESET))

////UartIoTFlags//////////////
#define IOT_ON     			0x00000001
#define IOT_INI_STR 		0x00000002
#define IOT_RSP_OK			0x00000004
#define IOT_RSP_ERROR		0x00000008

#define IOT_NEW_CMD 		0x00000010

void uart_iot_ini(void);
void uart_iot_parser (uint8_t *dato);
void tx_task();
void rx_task();
uint32_t uart_iot_send_cmd (uint8_t str[], uint32_t tiempo);
void uart_iot_send_str_l (uint8_t *str, int longitud);




#endif /* MAIN_UART_BLE_H_ */
