/*
 * misc.h
 *
 *  Created on: 21 ene. 2019
 *      Author: Antoni
 */

#ifndef MAIN_MISC_H_
#define MAIN_MISC_H_

#include "ctes.h"
#include "esp_system.h"

uint32_t str_cmp_upcase (uint8_t *cmp_str1, uint8_t *cmp_str2, uint32_t lon);
uint32_t str_len(uint8_t *str);
uint32_t str_cpy (uint8_t *cpy_str1, uint8_t *cpy_str2, uint32_t lon);


#endif /* MAIN_MISC_H_ */
