/*
 * main.h
 *
 *  Created on: 20 mar. 2019
 *      Author: Antoni
 */

#ifndef MAIN_MAIN_H_
#define MAIN_MAIN_H_

#include "esp_system.h"

void ble_indicate(uint8_t *valueptr, int len);
void flash_custom_data_set(uint8_t *str);



#endif /* MAIN_MAIN_H_ */
