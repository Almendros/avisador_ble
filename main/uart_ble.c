/*
 * uart_iot.c
 *
 *  Created on: 21 ene. 2019
 *      Author: Antoni
 */

#include "uart_ble.h"

#include "misc.h"


uint32_t UartIoTFlags = 0, UartIoTParserPtr = 0, UartIoTLen;
uint8_t UartIoTParserBuf[IOT_DATA_LONG];
xQueueHandle UartIoT2AvisadorQueue, UartIoTfromAvisadorQueue;


#define NUM_CMD_COM	2

const uint8_t STR_UART_IOT_RSP_OK [] =	{"OK"};
const uint8_t STR_UART_IOT_RSP_ERROR [] =	{"ERROR"};

const uint8_t* UART_IOT_RSP_STRINGS[NUM_CMD_COM] =
{
		STR_UART_IOT_RSP_OK, STR_UART_IOT_RSP_ERROR
};

const uint32_t UART_IOT_RSP_LONG[NUM_CMD_COM] =
{
		2, 5
};



void uart_iot_ini(void)
{
	const uart_config_t uart_config = {
			.baud_rate = 115200,
			.data_bits = UART_DATA_8_BITS,
			.parity = UART_PARITY_DISABLE,
			.stop_bits = UART_STOP_BITS_1,
			.flow_ctrl = UART_HW_FLOWCTRL_DISABLE
	};
	uart_param_config(UART_NUM_1, &uart_config);
	uart_set_pin(UART_NUM_1, TXD_PIN, RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
	// We won't use a buffer for sending data.
	uart_driver_install(UART_NUM_1, RX_BUF_SIZE * 2, 0, 0, NULL, 0);

//	gpio_config_t io_conf;
//	//disable interrupt
//	io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
//	//set as output mode
//	io_conf.mode = GPIO_MODE_OUTPUT;
//	//bit mask of the pins that you want to set,e.g.GPIO18/19
//	io_conf.pin_bit_mask = GPIO_BG96_PIN_SEL;
//	//disable pull-down mode
//	io_conf.pull_down_en = 0;
//	//disable pull-up mode
//	io_conf.pull_up_en = 0;
//	//configure GPIO with the given settings
//	//   gpio_set_level(GPIO_OUTPUT_IO_1, 0);
//
//	gpio_config(&io_conf);
//
//	gpio_set_level(GPIO_BG96_RESET, 0);
//
//	gpio_set_level(GPIO_BG96_PWRKEY, 0);
//	vTaskDelay(1000 / portTICK_PERIOD_MS);
//	gpio_set_level(GPIO_BG96_PWRKEY, 1);
//	vTaskDelay(1000 / portTICK_PERIOD_MS);
//	gpio_set_level(GPIO_BG96_PWRKEY, 0);
//	vTaskDelay(10000 / portTICK_PERIOD_MS);
}
/* ******************************************************************************************** */
void uart_iot_send_char (uint8_t car)
{
	xQueueSend (UartIoT2AvisadorQueue, &car, 0);
}
/* ******************************************************************************************** */
void uart_iot_send_str (uint8_t *str)
{
	while (*str != '\0')
	{
		xQueueSend(UartIoT2AvisadorQueue, str++, 0);
	}
}
/* ******************************************************************************************** */
void uart_iot_send_str_l (uint8_t *str, int longitud)
{
	while (longitud--)
	{
		xQueueSend (UartIoT2AvisadorQueue, str++, 0);
	}
}
/* ******************************************************************************************** */
void uart_iot_send_from_avisador_str_l (uint8_t *str, int longitud)
{
	while (longitud--)
	{
		xQueueSend (UartIoTfromAvisadorQueue, str++, 0);
	}
}
/* ******************************************************************************************** */
uint32_t uart_iot_send_cmd (uint8_t str[], uint32_t tiempo)
{
	//UartIoT_send_str ("AT");
	//static uint8_t Buffer[64];
	//sprintf((*char)Buffer,"AT%s\r",(*char)str);
	uart_iot_send_str (str);
	//UartIoT_send_str ("\r");
	CLR_FLAG (UartIoTFlags, IOT_RSP_OK | IOT_RSP_ERROR);
	if (tiempo == 0) return TRUE;
	while (tiempo)
	{
		vTaskDelay(1/ portTICK_PERIOD_MS);
		tiempo--;
		if (TST_FLAG (UartIoTFlags, IOT_RSP_OK))
			return TRUE;
		if (TST_FLAG (UartIoTFlags, IOT_RSP_ERROR))
			return FALSE;
	}
	return FALSE;
}
uint32_t uart_iot_buscar_rsp ()
{
	uint32_t i;
	for (i = 0; i < NUM_CMD_COM; i++)
	{
		if (str_cmp_upcase ((uint8_t*)UART_IOT_RSP_STRINGS[i], UartIoTParserBuf, UART_IOT_RSP_LONG [i]) == TRUE)
			return i;
	}
	return 0xFFFFFFFF;
}
void uart_iot_parser (uint8_t *dato)
{
	static const char *UART_IOT_PARSER = "UART_IOT_PARSER";
	for (int j = 0; dato[j] != '\0'; j++)
	{
		switch (dato[j])
		{
		case '\0':	break;
		case ' ':	break;

		case '\n':
		case '\r':
			if (TST_FLAG (UartIoTFlags, IOT_INI_STR))
			{
				UartIoTParserBuf[UartIoTParserPtr] = '\0';
				UartIoTLen = UartIoTParserPtr;
				UartIoTParserPtr = 0;
				CLR_FLAG (UartIoTFlags, IOT_INI_STR);
				SET_FLAG (UartIoTFlags, IOT_NEW_CMD);
			}
			break;
		default:
			SET_FLAG (UartIoTFlags, IOT_INI_STR);
			UartIoTParserBuf[UartIoTParserPtr] = dato[j];
			if (UartIoTParserPtr < (IOT_DATA_LONG - 1))
				UartIoTParserBuf[++UartIoTParserPtr] = '\0';
			break;
		}
		if (TST_FLAG (UartIoTFlags, IOT_NEW_CMD))
		{
			ESP_LOGI(UART_IOT_PARSER, "Nuevo comando %d bytes: '%s'", UartIoTLen, UartIoTParserBuf);
			CLR_FLAG (UartIoTFlags, IOT_NEW_CMD);
			uint32_t resp = uart_iot_buscar_rsp ();
			ESP_LOGI(UART_IOT_PARSER, "Respuesta %d", resp);
			switch (resp)
			{
				case 0:	SET_FLAG (UartIoTFlags, IOT_RSP_OK);	break;	// "OK"
				case 1:	SET_FLAG (UartIoTFlags, IOT_RSP_ERROR);	break;	// "ERROR"
	//			case 2:	UartIoT_fill_buf(UartIoTImei, 15);		break;	// "#CGSN" --> respuesta del IMEI(serial del modulo)
	//			case 3:	UartIoT_fill_buf(UartIoTCcid, 20);		break;	// "#CCID" --> respuesta de CCID(serial de la SIM)
	//			case 4:	UartIoT_pin ();					        break;	// "+CPIN" --> pin READY
	//			case 5:	UartIoT_rsp_cind ();				    break;	// "+CIND" indicadores GSM
	//			case 6:	UartIoT_csq();					        break;	// "+CSQ"
	//			case 7:	UartIoT_rsp_psnt();					    break;	// "#PSTN"--> Network type
	//			case 8:	UartIoT_cell_info();					break;	// "#SERVINFO"--> gprs?
	//			case 9:	UartIoT_connected();					break;	// "#CONNECTED"
	//			case 10: UartIoT_context_on();					break;	// "ALREADY CONTEXT ON"
	//			case 11: UartIoT_socket_st();					break;	// "socket st"
				//				case 12: UartGps_data_reciv();					break;	// "dades rebudes"
			}
			CLR_FLAG (UartIoTFlags, IOT_NEW_CMD);
			UartIoTParserPtr = 0;
		}

	}
}
/* ******************************************************************************************** */
/**
  * @brief Envia datos uart1
  * @param
  * @retval
  */
int sendData(const char* logName, const char* data)
{
	const int len = strlen(data);
	const int txBytes = uart_write_bytes(UART_NUM_1, data, len);
	ESP_LOGI(logName, "Wrote %d bytes", txBytes);
	return txBytes;
}
/* ******************************************************************************************** */
/**
  * @brief Task que gestiona los datos a enviar al avisador
  * @param
  * @retval
  */
void tx_task()
{
	static const char *TX_TASK_TAG = "TX_TASK";
	uint8_t car;
	int ptr = 0;
	esp_log_level_set(TX_TASK_TAG, ESP_LOG_INFO);
	uint8_t* data = (uint8_t*) malloc(IOT_DATA_LONG + 1);
	while (1)
	{
		uint32_t tam = uxQueueMessagesWaiting(UartIoT2AvisadorQueue);
		if (tam)
		{
			ptr = 0;
			while (xQueueReceive (UartIoT2AvisadorQueue, &car, 1) != errQUEUE_EMPTY)
			{
				data[ptr++] = car;
			}
			if (ptr)
			{
				data[ptr++] = '\0';
				sendData(TX_TASK_TAG, (const char*)data);
			}
		}
		vTaskDelay(10 / portTICK_PERIOD_MS);
	}
	free(data);
}
/* ******************************************************************************************** */
/**
  * @brief Task que gestiona los datos que llegan desde el avisador
  * @param
  * @retval
  */
void rx_task()
{
	static const char *RX_TASK_TAG = "RX_TASK";
	esp_log_level_set(RX_TASK_TAG, ESP_LOG_INFO);
	uint8_t* data = (uint8_t*) malloc(RX_BUF_SIZE+1);
	while (1)
	{
		const int rxBytes = uart_read_bytes(UART_NUM_1, data, RX_BUF_SIZE, 100 / portTICK_RATE_MS);
		if (rxBytes > 0)
		{
			data[rxBytes] = 0;
			ESP_LOGI(RX_TASK_TAG, "Read %d bytes: '%s'", rxBytes, data);
			ESP_LOG_BUFFER_HEXDUMP(RX_TASK_TAG, data, rxBytes, ESP_LOG_INFO);
			uart_iot_send_from_avisador_str_l(data, rxBytes);
			//uart_iot_parser (data);
		}
	}
	free(data);
}
/* ******************************************************************************************** */
