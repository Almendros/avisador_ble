/*
 * varios.c

 *
 *  Created on: 31 de ene. de 2017
 *      Author: francisco.almendros
 */
#include "misc.h"
/* ******************************************************************************************** */
uint32_t str_len(uint8_t *str)
{
	uint32_t i = 0;
	while (i < 0xFFFF)
	{
		if (*str == '\0')	break;
		str ++;
		i++;
	}
	return i;
}
/* ******************************************************************************************** */
uint32_t str_cpy (uint8_t *cpy_str1, uint8_t *cpy_str2, uint32_t lon)
{
	uint32_t i = 0;
	while (lon)
	{
		if (*cpy_str2 == '\0')
			break;
		*cpy_str1 = *cpy_str2;
		cpy_str1++;
		cpy_str2++;
		i++;
		lon--;
	}
	*cpy_str1 = '\0';
	return i;
}
/* ******************************************************************************************** */
uint8_t str_to_upcase (uint8_t car)
{
	if (car >= 'a')
	if (car <= 'z')
		return (car - 'a' + 'A');
	return car;
}
/* ******************************************************************************************** */
uint32_t str_cmp_upcase (uint8_t *cmp_str1, uint8_t *cmp_str2, uint32_t lon)
{
	for (;;)
	{
		if (str_to_upcase(*cmp_str1) != str_to_upcase(*cmp_str2)) break;
		cmp_str1++;
		cmp_str2++;
		if (--lon == 0)
			return TRUE;
		if (*cmp_str1 == '\0')
			if (*cmp_str2 == '\0')
				return TRUE;
		if (*cmp_str1 == '\0') return MAYBE;
		if (*cmp_str2 == '\0') return MAYBE;
	}
	return FALSE;
}
uint32_t str_cat (uint8_t *dst, uint8_t *org, uint32_t lon)
{
	uint32_t i = 0;
	i = str_len (dst);
	return str_cpy (&dst [i], org, lon);
}
/* ******************************************************************************************** */
void itodec (uint8_t *num, uint32_t val)
	{
	uint32_t aux, primer_digit, k;

	if (val == 0 || val > 65535)
	{
		num [0] = '0';
		num [1] = '\0';
		return;
	}
	primer_digit = 0;
	k = 10000;
	while (k)
	{
		aux = val / k;
		if (aux) primer_digit = 1;
		if (primer_digit)
		{
			*num =  '0' + aux;
			num++;
		}
		val %= k;
		k /= 10;
	}
	*num = '\0';
	}
/* ******************************************************************************************** */
uint32_t str2val (uint8_t *str)
{
	uint32_t val = 0;
	for (;;)
	{
		if ((*str >= '0') && (*str <= '9'))
		{
			val *= 10;
			val += (*str -'0');
		}
		else	break;
		str++;
	}
	return val;
}
/* ******************************************************************************************** */
uint32_t str_cmp (uint8_t *cmp_str1, uint8_t *cmp_str2, uint32_t lon, uint32_t conceros)
//a�adimos la opci� que acepte en cmp_str1 una cadena colo ceros y nos se salga, ser� el conceros TRUE
{
	for (;;)
	{
		if (*cmp_str1 != *cmp_str2)
			break;
		cmp_str1++;
		cmp_str2++;
		if (--lon == 0)
			return TRUE;
		if ((*cmp_str1 == '\0') && !conceros)
		if (*cmp_str2 == '\0')
			return TRUE;
		if ((*cmp_str1 == '\0') && !conceros) return MAYBE;
		if (*cmp_str2 == '\0') return MAYBE;
		}
	return FALSE;
}
/* ******************************************************************************************** */
uint8_t Bcd2ToByte(uint8_t Value)
{
  uint32_t tmp = 0;
  tmp = ((uint8_t)(Value & (uint8_t)0xF0) >> (uint8_t)0x4) * 10;
  return (tmp + (Value & (uint8_t)0x0F));
}
/* ******************************************************************************************** */
void str_cpy_with_esp(uint8_t *cpy_str_dest, uint8_t *cpy_str1, uint8_t *cpy_str2, uint8_t *esp)
{
	//char dot[] = ".";
	int lon1 = str_len(cpy_str1);
	int lon2 = str_len(cpy_str2);
	str_cpy (cpy_str_dest, cpy_str1, lon1);
	str_cat (cpy_str_dest, esp, 1);
	str_cat (cpy_str_dest, cpy_str2, lon2);
}

/* ******************************************************************************************** */
