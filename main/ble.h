/*
 * ble.h
 *
 *  Created on: 25 feb. 2019
 *      Author: Antoni
 */

#ifndef MAIN_BLE_H_
#define MAIN_BLE_H_

#include "ctes.h"
#include "esp_system.h"

void ble_state_machine_task();
void ble_gat_connected(uint32_t st);

#define BLE_DATA_LONG  		512

#define BLE_CMD_AVIS_BOOT 			0x20  //el avisador ha arrancado y env�a este comando

enum
{
	BLE_ST_BLE_BOOT = 0,
	BLE_ST_BLE_AVIS_UP = 1,
	BLE_ST_BLE_CONNECTED = 2,
	BLE_ST_BLE_DISCONNECTED = 3
};


#define BLE_COM_FRAME_INI     			0x01
#define BLE_COM_FRAME_FIN     			0x02


#define BLE_PROG_FLG_INI     			0x00000001
#define BLE_PROG_FLG_AVIS_BOOT  		0x00000010


#endif /* MAIN_BLE_H_ */
